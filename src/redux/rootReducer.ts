import { combineReducers } from "redux";

import commentReducer from "./modules/comment/reducer";
import postReducer from "./modules/post/reducer";
import settingsReducer from "./modules/settings/reducer";
import subredditReducer from "./modules/subreddit/reducer";

import RootState from "./rootState";

const rootReducer = combineReducers<RootState>({
  subreddit: subredditReducer,
  post: postReducer,
  settings: settingsReducer,
  comment: commentReducer,
});

export default rootReducer;
