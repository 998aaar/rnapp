import { all, AllEffect, fork } from "redux-saga/effects";

import {
  commentSaga,
  postSaga,
  subredditSaga,
} from "./sagas";

function* rootSaga() {
  yield all([
    fork(postSaga),
    fork(subredditSaga),
    fork(commentSaga),
  ]);
}

export default rootSaga;
