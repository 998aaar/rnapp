import { bindActionCreators } from "redux";
import { ActionType, createAction } from "typesafe-actions";

import { Subreddit } from "../../../types/subreddit";

export const postActions = {
  loadTopPostsFromSubredditRequest: createAction("[POST] LOAD_TOP_POSTS_FROM_SUBREDDIT_REQUEST", (resolve) => {
    return (subredditName: Subreddit["name"]) => resolve({ subredditName });
  }),

  loadTopPostsFromSubredditSuccess: createAction("[POST] LOAD_TOP_POSTS_FROM_SUBREDDIT_SUCCESS", (resolve) => {
    return (subredditName: string, posts: any[]) => resolve({ subredditName, posts });
  }),

  loadTopPostsFromSubredditError: createAction("[POST] LOAD_TOP_POSTS_FROM_SUBREDDIT_ERROR"),
};

export type PostAction = ActionType<typeof postActions>;
