import { getType } from "typesafe-actions";

import { Post, PostCollection } from "../../../types/post";
import { PostAction, postActions } from "./actions";

export interface PostState {
  posts: PostCollection;
  fetchingPosts: boolean;
  errorFetchingPosts: boolean;
}

export const initialPostState: PostState = {
  posts: {},
  fetchingPosts: false,
  errorFetchingPosts: false,
};

const subredditReducer = (state = initialPostState, action: PostAction) => {
  switch (action.type) {
    case getType(postActions.loadTopPostsFromSubredditRequest):
      return {
        ...state,
        fetchingPosts: true,
      };
    case getType(postActions.loadTopPostsFromSubredditSuccess):
      const posts: PostCollection = {};

      action.payload.posts.forEach((post) => {
        // Create post with data from API
        const p: Post = {
          id: post.data.id,
          title: post.data.title,
          comments: post.data.num_comments,
          thumbnail_url: post.data.thumbnail,
          points: post.data.score,
          subreddit: post.data.subreddit,
        };

        // Push post to post collection
        posts[post.data.id] = { ...posts[post.data.id], ...p };
      });

      return {
        ...state,
        posts: {
          ...state.posts,
          ...posts,
        },
        fetchingPosts: false,
        errorFetchingPosts: false,
      };
    case getType(postActions.loadTopPostsFromSubredditError):
      return {
        ...state,
        fetchingPosts: false,
        errorFetchingPosts: true,
      };
    default:
      return state;
  }
};

export default subredditReducer;
