import { getType } from "typesafe-actions";

import { SettingsAction, settingsActions } from "./actions";

export interface SettingsState {
  darkMode: boolean;
}

export const initialSettingsState: SettingsState = {
  darkMode: false,
};

const settingsReducer = (state = initialSettingsState, action: SettingsAction) => {
  switch (action.type) {
    case getType(settingsActions.setDarkMode):
      return {
        ...state,
        darkMode: !state.darkMode,
      };
    default:
      return state;
  }
};

export default settingsReducer;
