import { bindActionCreators } from "redux";
import { ActionType, createAction } from "typesafe-actions";

export const settingsActions = {
  setDarkMode: createAction("[SETTINGS] SET_DARK_MDOE"),
};

export type SettingsAction = ActionType<typeof settingsActions>;
