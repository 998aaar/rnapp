import { getType } from "typesafe-actions";

import { Comment, CommentCollection } from "../../../types/comment";
import { CommentAction, commentActions } from "./actions";

export interface CommentState {
  comments: CommentCollection;
  fetchingComments: boolean;
  errorFetchingComments: boolean;
}

export const initialCommentState: CommentState = {
  comments: {},
  fetchingComments: false,
  errorFetchingComments: false,

};

const commentReducer = (state = initialCommentState, action: CommentAction) => {
  switch (action.type) {
    case getType(commentActions.loadTopCommentsFromPostRequest):
      return {
        ...state,
        fetchingComments: true,
      };
    case getType(commentActions.loadTopCommentsFromPostSuccess):
      const comments: CommentCollection = {};

      action.payload.comments.forEach((comment) => {
        // Create the comment with the data returned from the API
        const c: Comment = {
          id: comment.data.id,
          author: comment.data.author,
          body: comment.data.body,
          points: comment.data.score,
          postId: comment.data.parent_id.substring(3),
        };

        // Push the comment to the comment collection
        comments[comment.data.id] = { ...comments[comment.data.id], ...c };
      });

      return {
        ...state,
        comments,
        fetchingComments: false,
        errorFetchingComments: false,
      };
    case getType(commentActions.loadTopCommentsFromPostError):
      return {
        ...state,
        fetchingComments: false,
        errorFetchingComments: true,
      };
    default:
      return state;
  }
};

export default commentReducer;
