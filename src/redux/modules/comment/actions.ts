import { bindActionCreators } from "redux";
import { ActionType, createAction } from "typesafe-actions";

export const commentActions = {
  loadTopCommentsFromPostRequest: createAction("[COMMENT] LOAD_TOP_COMMENTS_FROM_POST_REQUEST", (resolve) => {
    return (subreddit: string, postId: string) => resolve({ subreddit, postId });
  }),

  loadTopCommentsFromPostSuccess: createAction("[COMMENT] LOAD_TOP_COMMENTS_FROM_POST_SUCCESS", (resolve) => {
    return (comments: any[]) => resolve({ comments });
  }),

  loadTopCommentsFromPostError: createAction("[COMMENT] LOAD_TOP_COMMENTS_FROM_POST_ERROR"),
};

export type CommentAction = ActionType<typeof commentActions>;
