import { bindActionCreators } from "redux";
import { ActionType, createAction } from "typesafe-actions";

import { Subreddit } from "../../../types/subreddit";

export const subredditActions = {
  loadDefaultSubredditsRequest: createAction("[SUBREDDIT] LOAD_DEFAULT_SUBREDDITS_REQUEST"),

  loadDefaultSubredditsSuccess: createAction("[SUBREDDIT] LOAD_DEFAULT_SUBREDDITS_SUCCESS", (resolve) => {
    return (subreddits: any[]) => resolve({ subreddits });
  }),

  loadDefaultSubredditsError: createAction("[SUBREDDIT] LOAD_DEFAULT_SUBREDDITS_ERROR"),
};

export type SubredditAction = ActionType<typeof subredditActions>;
