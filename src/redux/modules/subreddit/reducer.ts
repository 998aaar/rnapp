import { getType } from "typesafe-actions";

import { Subreddit } from "../../../Types/subreddit";
import { SubredditAction, subredditActions } from "./actions";

export interface SubredditState {
  subreddits: Subreddit[];
  fetchingSubreddits: boolean;
  errorFetchingSubreddits: boolean;
}

export const initialSubredditState: SubredditState = {
  subreddits: [],
  fetchingSubreddits: false,
  errorFetchingSubreddits: false,
};

const subredditReducer = (state = initialSubredditState, action: SubredditAction) => {
  switch (action.type) {
    case getType(subredditActions.loadDefaultSubredditsRequest):
      return {
        ...state,
        fetchingSubreddits: true,
      };
    case getType(subredditActions.loadDefaultSubredditsSuccess):
      const subreddits: Subreddit[] = [];

      action.payload.subreddits.forEach((subreddit) => {
        // Create subreddit with data returned by the API
        const sub: Subreddit = {
          id: subreddit.data.id,
          name: subreddit.data.display_name_prefixed,
          description: subreddit.data.public_description,
          color: subreddit.data.primary_color,
        };

        // In this case we push since our use case doesn't
        // require looking fro subreddit by ID
        subreddits.push(sub);
      });

      return {
        ...state,
        subreddits,
        fetchingSubreddits: false,
        errorFetchingSubreddits: false,
      };
    case getType(subredditActions.loadDefaultSubredditsError):
      return {
        ...state,
        fetchingSubreddits: false,
        errorFetchingSubreddits: true,
      };
    default:
      return state;
  }
};

export default subredditReducer;
