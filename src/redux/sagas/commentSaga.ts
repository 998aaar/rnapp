import axios from "axios";
import {
  all,
  call,
  fork,
  put,
  takeLatest,
} from "redux-saga/effects";
import { getType } from "typesafe-actions";

import { commentActions } from "../modules/comment/actions";

export function* watchLoadTopCommentsFromPost() {
  yield takeLatest(getType(commentActions.loadTopCommentsFromPostRequest), handleLoadTopCommentsFromPostRequest);
}

function* handleLoadTopCommentsFromPostRequest(action: any) {
  const { subreddit, postId } = action.payload;
  const url =
    `https://www.reddit.com/${subreddit}/comments/${postId}.json`;

  try {
    const request = yield call(axios.get, url);
    yield put(
      commentActions.loadTopCommentsFromPostSuccess(request.data[1].data.children),
    );
  } catch (error) {
    yield put(commentActions.loadTopCommentsFromPostError());
  }
}

export default function* commentRequestWatchers() {
  yield all([
    fork(watchLoadTopCommentsFromPost),
  ]);
}
