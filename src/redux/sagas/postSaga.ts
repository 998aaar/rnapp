import axios from "axios";
import {
  all,
  call,
  fork,
  put,
  takeLatest,
} from "redux-saga/effects";
import { getType } from "typesafe-actions";

import { postActions } from "../modules/post/actions";

export function* watchLoadTopPostsFromSubreddit() {
  yield takeLatest(getType(postActions.loadTopPostsFromSubredditRequest), handleLoadTopPostsFromSubreddit);
}

function* handleLoadTopPostsFromSubreddit(action: any) {
  const { subredditName } = action.payload;
  const url =
    `https://www.reddit.com/${subredditName}/top.json`;

  try {
    const request = yield call(axios.get, url);
    yield put(
      postActions.loadTopPostsFromSubredditSuccess(subredditName, request.data.data.children),
    );
  } catch (error) {
    yield put(postActions.loadTopPostsFromSubredditError());
  }
}

export default function* postRequestWatchers() {
  yield all([
    fork(watchLoadTopPostsFromSubreddit),
  ]);
}
