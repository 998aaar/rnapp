import axios from "axios";
import {
  all,
  call,
  fork,
  put,
  takeLatest,
} from "redux-saga/effects";
import { getType } from "typesafe-actions";

import { subredditActions } from "../modules/subreddit/actions";

export function* watchLoadDefaultSubreddits() {
  yield takeLatest(getType(subredditActions.loadDefaultSubredditsRequest), handleLoadDefaultSubredditsRequest);
}

function* handleLoadDefaultSubredditsRequest() {
  const url =
    "https://www.reddit.com/subreddits/default.json";

  try {
    const request = yield call(axios.get, url);
    if (request.data.data.children.length > 0) {
      yield put(subredditActions.loadDefaultSubredditsSuccess(request.data.data.children));
    }
  } catch (error) {
    yield put(subredditActions.loadDefaultSubredditsError());
  }
}

export default function* subredditRequestWatchers() {
  yield all([
    fork(watchLoadDefaultSubreddits),
  ]);
}
