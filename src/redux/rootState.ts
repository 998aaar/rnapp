import { CommentState, initialCommentState } from "./modules/comment/reducer";
import { initialPostState, PostState } from "./modules/post/reducer";
import { initialSettingsState, SettingsState } from "./modules/settings/reducer";
import { initialSubredditState, SubredditState } from "./modules/subreddit/reducer";

export const initialState: RootState = {
  subreddit: initialSubredditState,
  post: initialPostState,
  settings: initialSettingsState,
  comment: initialCommentState,
};

export default interface RootState {
  subreddit: SubredditState;
  post: PostState;
  settings: SettingsState;
  comment: CommentState;
}
