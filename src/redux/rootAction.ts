import { CommentAction } from "./modules/comment/actions";
import { PostAction } from "./modules/post/actions";
import { SettingsAction } from "./modules/settings/actions";
import { SubredditAction } from "./modules/subreddit/actions";

type RootAction =
  | SubredditAction
  | SettingsAction
  | CommentAction
  | PostAction;

export default RootAction;
