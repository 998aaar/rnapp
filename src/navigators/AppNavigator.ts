import React, { SFC } from "react";
import {
  Text,
  View,
} from "react-native";
import { createBottomTabNavigator } from "react-navigation";

import HomeNavigator from "./HomeNavigator";
import SettingsNavigator from "./SettingsNavigator";

export const AppNavigatorHOC = (darkMode: boolean) => (
  createBottomTabNavigator({
    Home: {
      screen: HomeNavigator,
    },
    Settings: {
      screen: SettingsNavigator,
    },
  }, {
      tabBarOptions: {
        tabStyle: {
          backgroundColor: darkMode ? "#222222" : "#ffffff",
        },
        style: {
          borderTopColor: darkMode ? "#000000" : "#aaaaaa",
        },
        labelStyle: {
          fontSize: 14,
        },
        activeTintColor: darkMode ? "#ffffff" : "#000000",
        inactiveTintColor: darkMode ? "rgba(255, 255, 255, 0.5)" : "rgba(0, 0, 0, 0.4)",
        showIcon: false,
      },
    })
);

export default AppNavigatorHOC;
