import { createStackNavigator } from "react-navigation";

import {
  SettingsScreen,
  withStackScreenHOC,
} from "../screens";

const SettingsNavigator = createStackNavigator({
  Settings: withStackScreenHOC(SettingsScreen),
}, {
    navigationOptions: {
      headerBackTitle: null,
    },
  });

export default SettingsNavigator;
