import { createStackNavigator } from "react-navigation";

import {
  HomeScreen,
  PostDetailScreen,
  SubredditScreen,
  withStackScreenHOC,
} from "../screens";

const HomeNavigator = createStackNavigator({
  Main: withStackScreenHOC(HomeScreen),
  Subreddit: withStackScreenHOC(SubredditScreen),
  Post: withStackScreenHOC(PostDetailScreen),
}, {
    navigationOptions: {
      headerBackTitle: null,
    },
  });

export default HomeNavigator;
