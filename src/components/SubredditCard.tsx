import React, { SFC } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
} from "react-native";

import { Subreddit } from "../types/subreddit";

interface SubredditCardProps {
  darkMode: boolean;
  onPress: () => void;
}

const SubredditCard: SFC<SubredditCardProps & Subreddit> = (props) => {
  const styles = props.darkMode ? darkStyles : lightStyles;

  return (
    <TouchableOpacity
      onPress={() => props.onPress()}
      style={[styles.card, { borderLeftColor: props.color, borderLeftWidth: 5 }]}>
      <Text style={styles.cardName}>{props.name}</Text>
      <Text style={styles.cardDescription}>{props.description}</Text>
    </TouchableOpacity>
  );
};

const lightStyles = StyleSheet.create({
  card: {
    marginBottom: 1,
    padding: 20,
    backgroundColor: "#ffffff",
  },
  cardName: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#000000",
  },
  cardDescription: {
    color: "hsl(0, 0%, 50%)",
  },
});

const darkStyles = StyleSheet.create({
  card: {
    marginBottom: 1,
    padding: 20,
    backgroundColor: "#222222",
  },
  cardName: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#ffffff",
  },
  cardDescription: {
    color: "hsl(0, 0%, 75%)",
  },
});

export default SubredditCard;
