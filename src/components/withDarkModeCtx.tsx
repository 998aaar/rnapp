import React, { ComponentType } from "react";

import { DarkModeCtx } from "../contexts";

const withDarkModeCtx = <T extends {}>(WrappedComponent: ComponentType<T>) => (
  (props: any) => (
    <DarkModeCtx.Consumer>
      {(darkMode: boolean) => (
        <WrappedComponent {...props} darkMode={darkMode} />
      )}
    </DarkModeCtx.Consumer>
  )
);

export default withDarkModeCtx;
