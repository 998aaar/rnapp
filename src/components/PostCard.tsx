import React, { SFC } from "react";
import {
  Button,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";

import { Post } from "../types/post";

const defaultImage = require("../resources/images/defaultImage.png");

interface SubredditCardProps {
  darkMode: boolean;
  onPress: () => void;
}

const PostCard: SFC<SubredditCardProps & Post> = (props) => {
  const styles = props.darkMode ? darkStyles : lightStyles;

  return (
    <TouchableOpacity
      onPress={() => props.onPress()}
      style={styles.card}>
      {props.thumbnail_url !== "" &&
        <View style={styles.cardThumbnail}>
          <Image
            style={{ width: 80, height: 80 }}
            defaultSource={defaultImage}
            source={{ uri: props.thumbnail_url }} />
        </View>
      }
      <View style={styles.cardDetails}>
        <View style={styles.cardInfo}>
          <Text style={styles.cardPoints}>{props.points} points</Text>
          <Text style={styles.cardPoints}>{props.comments} {props.comments === 1 ? "comment" : "comments"}</Text>
        </View>
        <Text style={styles.cardTitle}>{props.title}</Text>
      </View>
    </TouchableOpacity>
  );
};

const lightStyles = StyleSheet.create({
  card: {
    padding: 20,
    flex: 1,
    flexDirection: "row",
    marginBottom: 1,
    backgroundColor: "#ffffff",
  },
  cardThumbnail: {
    overflow: "hidden",
    width: 80,
    height: 80,
    borderRadius: 10,
    marginRight: 20,
  },
  cardDetails: {
    flex: 1,
  },
  cardInfo: {
    marginBottom: 5,
    flexDirection: "row",
    flexWrap: "wrap",
  },
  cardPoints: {
    marginRight: 10,
    fontSize: 12,
    color: "hsl(0, 0%, 50%)",
  },
  cardTitle: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#000000",
  },
});

const darkStyles = StyleSheet.create({
  card: {
    padding: 20,
    flex: 1,
    flexDirection: "row",
    marginBottom: 1,
    backgroundColor: "#222222",
  },
  cardThumbnail: {
    overflow: "hidden",
    width: 80,
    height: 80,
    borderRadius: 10,
    marginRight: 20,
  },
  cardDetails: {
    flex: 1,
  },
  cardInfo: {
    marginBottom: 5,
    flexDirection: "row",
    flexWrap: "wrap",
  },
  cardPoints: {
    marginRight: 10,
    fontSize: 12,
    color: "hsl(0, 0%, 75%)",
  },
  cardTitle: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#ffffff",
  },
});

export default PostCard;
