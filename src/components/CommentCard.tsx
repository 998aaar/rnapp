import React, { SFC } from "react";
import {
  Button,
  Image,
  StyleSheet,
  Text,
  View,
} from "react-native";

import { Comment } from "../types/comment";
import Omit from "../utils/omit_type";

interface CommentCardProps {
  darkMode: boolean;
}

const CommentCard: SFC<CommentCardProps & Omit<Comment, "postId">> = (props) => {
  const styles = props.darkMode ? darkStyles : lightStyles;

  return (
    <View style={styles.card}>
      <View style={styles.cardAuthor}>
        <Text style={styles.cardTitle}>{props.author}</Text>
        <View style={styles.cardPoints}>
          <Text style={styles.cardPointsText}>{props.points} points</Text>
        </View>
      </View>
      <Text style={styles.cardBody}>{props.body}</Text>
    </View>
  );
};

const lightStyles = StyleSheet.create({
  card: {
    padding: 20,
    flex: 1,
    marginBottom: 1,
    backgroundColor: "#ffffff",
  },
  cardAuthor: {
    flex: 1,
    flexDirection: "row",
  },
  cardPoints: {
    marginLeft: 10,
    flexDirection: "row",
    flexWrap: "wrap",
  },
  cardPointsText: {
    marginRight: 10,
    fontSize: 12,
    color: "hsl(0, 0%, 50%)",
  },
  cardTitle: {
    fontWeight: "bold",
    color: "#000000",
  },
  cardBody: {
    marginTop: 10,
    color: "#000000",
  },
});

const darkStyles = StyleSheet.create({
  card: {
    padding: 20,
    flex: 1,
    marginBottom: 1,
    backgroundColor: "#222222",
  },
  cardAuthor: {
    flex: 1,
    flexDirection: "row",
  },
  cardPoints: {
    marginLeft: 10,
    flexDirection: "row",
    flexWrap: "wrap",
  },
  cardPointsText: {
    marginRight: 10,
    fontSize: 12,
    color: "hsl(0, 0%, 75%)",
  },
  cardTitle: {
    fontWeight: "bold",
    color: "#ffffff",
  },
  cardBody: {
    marginTop: 10,
    color: "#ffffff",
  },
});

export default CommentCard;
