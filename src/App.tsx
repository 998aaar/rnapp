import React, { Component, createContext, SFC } from "react";
import {
    createBottomTabNavigator, createStackNavigator,
} from "react-navigation";
import { connect, Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";

import { DarkModeCtx } from "./contexts";
import AppNavigatorHOC from "./navigators/AppNavigator";
import RootState from "./redux/rootState";
import * as settingsSelectors from "./selectors/settings";
import persistedStore from "./store";

type AppState = ReturnType<typeof mapStateToProps>;

const {
    store,
    persistor,
} = persistedStore;

class PersistedApp extends Component<AppState> {
    public render() {
        const { darkMode } = this.props;

        // We sue a "HOC" to inject the darkMode value to the
        // AppNavigator so we can change the color of the bottom tab.
        const AppNavigator = AppNavigatorHOC(darkMode);

        return (
            <PersistGate loading={null} persistor={persistor}>
                <DarkModeCtx.Provider value={darkMode}>
                    <AppNavigator />
                </DarkModeCtx.Provider>
            </PersistGate>
        );
    }
}

const mapStateToProps = (state: RootState) => ({
    darkMode: settingsSelectors.selectDarkMode(state),
});

const App = connect(mapStateToProps)(PersistedApp);

export default () => (
    <Provider store={store}>
        <App />
    </Provider>
);
