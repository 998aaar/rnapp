export interface Comment {
  id: string;
  author: string;
  body: string;
  points: number;
  postId: string;
}

export interface CommentCollection {
  [commentId: string]: Comment;
}
