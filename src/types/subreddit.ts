export interface Subreddit {
  id: string;
  name: string;
  color: string;
  description: string;
}
