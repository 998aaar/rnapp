export interface Post {
  id: string;
  title: string;
  thumbnail_url: string;
  comments: number;
  points: number;
  subreddit: string;
}

export interface PostCollection {
  [postId: string]: Post;
}
