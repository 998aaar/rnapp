import { createSelector } from "reselect";

import RootState from "../redux/rootState";

// Select post state from root state
const selectPostState = (state: RootState) =>
  state.post;

// Selects subreddit name from the navigation params
const selectSubredditNameFromProps = (state: RootState, props: any): string =>
  props.navigation.state.params.name;

// With the result from selectSubredditNameFromProps whe find every
// post that belongs to the subreddit
export const selectSubredditPosts = createSelector(
  [selectPostState, selectSubredditNameFromProps],
  (state, id) =>
    Object.values(state.posts).filter((e) => `r/${e.subreddit}` === id),
);

// Select the fetchingPosts boolean from the post state
export const selectFetchingPosts = createSelector(
  [selectPostState], (state) =>
    state.fetchingPosts,
);

// Select the errorFetchingPosts boolean from the post state
export const selectErrorFetchingPosts = createSelector(
  [selectPostState], (state) =>
    state.errorFetchingPosts,
);
