import { createSelector } from "reselect";

import RootState from "../redux/rootState";

// Select the subreddit state from root state
const selectSubredditState = (state: RootState) =>
  state.subreddit;

// Select the fetchingSubreddits boolean from the subreddit state
export const selectFetchingSubreddits = createSelector(
  [selectSubredditState], (state) =>
    state.fetchingSubreddits,
);

// Select the errorFetchingSubreddits boolean from the subreddit state
export const selectErrorFetchingSubreddits = createSelector(
  [selectSubredditState], (state) =>
    state.errorFetchingSubreddits,
);

// Select every subreddit from the state
export const selectSubreddits = createSelector(
  [selectSubredditState], (state) =>
    state.subreddits,
);
