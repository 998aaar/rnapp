import { createSelector } from "reselect";

import RootState from "../redux/rootState";

// Select the comments state from the root
const selectCommentState = (state: RootState) =>
  state.comment;

// Select postId that comes from the navigation params
const selectPostIdFromProps = (_: RootState, props: any): string =>
  props.navigation.state.params.postId;

// Find every comment that has a parent postId that matches the one
// from selectPostIdFromProps
export const selectPostComments = createSelector(
  [selectCommentState, selectPostIdFromProps],
  (state, id) =>
    Object.values(state.comments).filter((e) => e.postId === id),
);

// Select the fetchingComments property from our comment state
export const selectFetchingComments = createSelector(
  [selectCommentState], (state) =>
    state.fetchingComments,
);

// Select the error boolean from the comment state
export const selectErrorFetchingComments = createSelector(
  [selectCommentState], (state) =>
    state.errorFetchingComments,
);
