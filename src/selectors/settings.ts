import { createSelector } from "reselect";

import RootState from "../redux/rootState";

// Select settings state from root state
const selectSettingsState = (state: RootState) =>
  state.settings;

// Select the darkMode property from the settings state
export const selectDarkMode = createSelector(
  [selectSettingsState], (state) =>
    state.darkMode,
);
