import { createContext } from "react";

// The default value is set to false but it can also be
// an object { darkMode: false }
export default createContext(false);
