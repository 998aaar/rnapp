import { applyMiddleware, createStore } from "redux";
import { PersistConfig, persistReducer, persistStore } from "redux-persist";
import { createWhitelistFilter } from "redux-persist-transform-filter";
import storage from "redux-persist/lib/storage";
import createSagaMiddleware from "redux-saga";

import RootAction from "./redux/rootAction";
import rootReducer from "./redux/rootReducer";
import rootSaga from "./redux/rootSaga";
import RootState from "./redux/rootState";

const persistConfig = {
  key: "root",
  storage,
  // Persist only the settings reducer since that's what we
  // want to "remember" everytime we use the app.
  whitelist: ["settings"],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const sagaMiddleware = createSagaMiddleware();
const store = createStore<RootState, RootAction, {}, {}>(persistedReducer, applyMiddleware(sagaMiddleware));
const persistor = persistStore(store);

sagaMiddleware.run(rootSaga);

export default { store, persistor };
