import React, { Component } from "react";
import {
  ActivityIndicator,
  Button,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import { NavigationScreenProps } from "react-navigation";
import { connect } from "react-redux";
import { bindActionCreators, Dispatch } from "redux";

import { CommentCard, withDarkModeCtx } from "../components";
import { commentActions } from "../redux/modules/comment/actions";
import RootAction from "../redux/rootAction";
import RootState from "../redux/rootState";
import * as commentSelectors from "../selectors/comment";
import { Comment } from "../types/comment";

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = ReturnType<typeof mapDispatchToProps>;
type PostDetailScreenProps =
  StateProps & DispatchProps & NavigationScreenProps & { darkMode: boolean };

interface PostDetailScreenState {
  subreddit: string;
  postId: string;
}

class PostDetailScreen extends Component<PostDetailScreenProps, PostDetailScreenState> {
  public componentWillMount() {
    const {
      navigation,
    } = this.props;

    this.setState({
      subreddit: navigation.state.params && navigation.state.params.subreddit,
      postId: navigation.state.params && navigation.state.params.postId,
    });
  }

  public componentDidMount() {
    const {
      comments,
      loadTopCommentsFromPost,
    } = this.props;

    if (Object.keys(comments).length === 0) {
      loadTopCommentsFromPost(this.state.subreddit, this.state.postId);
    }
  }

  public render() {
    const {
      fetchingComments,
      errorFetchingComments,
      comments,
    } = this.props;

    const styles = this.getStyles();

    return (
      <View style={styles.view}>
        {fetchingComments &&
          this.renderActivityIndicator()
        }
        {!fetchingComments && errorFetchingComments &&
          this.renderFetchingErrorMessage()
        }
        {!fetchingComments && Object.keys(comments).length > 0 &&
          this.renderComments(comments)
        }
      </View>
    );
  }

  private getStyles() {
    const { darkMode } = this.props;
    return darkMode ? darkStyles : lightStyles;
  }

  private renderActivityIndicator() {
    const styles = this.getStyles();

    return (
      <View style={styles.activityIndicator}>
        <ActivityIndicator size="large" />
      </View>
    );
  }
  private renderFetchingErrorMessage() {
    const { subreddit, postId } = this.state;
    const { loadTopCommentsFromPost } = this.props;
    const styles = this.getStyles();

    return (
      <View>
        <Text style={styles.errorText}>
          There was a problem loading posts from {subreddit}.
          Please try again.
          </Text>
        <Button
          title="Retry"
          onPress={() => loadTopCommentsFromPost(subreddit, postId)} />
      </View>
    );
  }

  private renderComments(comments: Comment[]) {
    const { subreddit, postId } = this.state;
    const { darkMode, loadTopCommentsFromPost } = this.props;
    const styles = this.getStyles();

    return (
      <ScrollView style={styles.scrollView}>
        <Button
          onPress={() => loadTopCommentsFromPost(subreddit, postId)}
          title="Refresh comments" />
        {
          Object.values(comments).map((comment) => {
            if (comment.author) {
              return (
                <CommentCard
                  darkMode={darkMode}
                  key={comment.id}
                  id={comment.id}
                  author={comment.author}
                  body={comment.body}
                  points={comment.points} />
              );
            }

            return null;
          })
        }
      </ScrollView>
    );
  }
}

const lightStyles = StyleSheet.create({
  view: {
    flex: 1,
  },
  scrollView: {
    flex: 1,
  },
  activityIndicator: {
    flex: 1,
    alignSelf: "center",
    justifyContent: "center",
  },
  errorText: {
    textAlign: "center",
    padding: 20,
  },
});

const darkStyles = StyleSheet.create({
  view: {
    flex: 1,
    backgroundColor: "#111111",
  },
  scrollView: {
    flex: 1,
  },
  activityIndicator: {
    flex: 1,
    alignSelf: "center",
    justifyContent: "center",
  },
  errorText: {
    textAlign: "center",
    padding: 20,
    color: "hsl(0, 0%, 75%)",
  },
});

const mapStateToProps = (state: RootState, props: NavigationScreenProps) => ({
  fetchingComments: commentSelectors.selectFetchingComments(state),
  errorFetchingComments: commentSelectors.selectErrorFetchingComments(state),
  comments: commentSelectors.selectPostComments(state, props),
});

const mapDispatchToProps = (dispatch: Dispatch<RootAction>) =>
  bindActionCreators({
    loadTopCommentsFromPost: commentActions.loadTopCommentsFromPostRequest,
  }, dispatch);

export default withDarkModeCtx(connect(mapStateToProps, mapDispatchToProps)(PostDetailScreen));
