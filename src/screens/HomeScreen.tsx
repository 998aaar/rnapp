import React, { Component } from "react";
import {
  ActivityIndicator,
  Button,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import { NavigationScreenProps } from "react-navigation";
import { connect } from "react-redux";
import { bindActionCreators, Dispatch } from "redux";

import {
  SubredditCard,
  withDarkModeCtx,
} from "../components";
import { subredditActions } from "../redux/modules/subreddit/actions";
import RootAction from "../redux/rootAction";
import RootState from "../redux/rootState";
import * as subredditSelectors from "../selectors/subreddit";
import { Subreddit } from "../types/subreddit";

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = ReturnType<typeof mapDispatchToProps>;
type HomeScreenProps =
  StateProps & DispatchProps & NavigationScreenProps & { darkMode: boolean };

class HomeScreen extends Component<HomeScreenProps> {
  public componentDidMount() {
    const {
      darkMode,
    } = this.props;

    this.props.loadDefaultSubreddits();
  }

  public render() {
    const {
      fetchingSubreddits,
      errorFetchingSubreddits,
      subreddits,
    } = this.props;

    const styles = this.getStyles();

    return (
      <View style={styles.view}>
        {fetchingSubreddits &&
          this.renderActivitiyIndicator()
        }
        {!fetchingSubreddits && errorFetchingSubreddits &&
          this.renderFetchingErrorMessage()
        }
        {subreddits.length > 0 &&
          this.renderSubredditCards(subreddits)
        }
      </View>
    );
  }

  private getStyles() {
    const { darkMode } = this.props;
    return darkMode ? darkStyles : lightStyles;
  }

  private renderActivitiyIndicator() {
    const styles = this.getStyles();

    return (
      <View style={styles.activityIndicator}>
        <ActivityIndicator size="large" />
      </View>
    );
  }

  private renderFetchingErrorMessage() {
    const styles = this.getStyles();

    return (
      <View>
        <Text style={styles.errorText}>
          There was a problem loading the default subreddits. Please try again.
          </Text>
        <Button
          title="Retry"
          onPress={() => this.props.loadDefaultSubreddits()} />
      </View>
    );
  }

  private renderSubredditCards(subreddits: Subreddit[]) {
    const { darkMode, navigation: { navigate } } = this.props;
    const styles = this.getStyles();

    return (
      <ScrollView style={styles.scrollView}>
        {
          subreddits.map((subreddit) => (
            <SubredditCard
              darkMode={darkMode}
              onPress={() => navigate("Subreddit", { name: subreddit.name })}
              key={subreddit.id}
              id={subreddit.id}
              name={subreddit.name}
              description={subreddit.description}
              color={subreddit.color} />
          ))
        }
      </ScrollView>
    );
  }
}

const lightStyles = StyleSheet.create({
  view: {
    flex: 1,
  },
  scrollView: {
    flex: 1,
  },
  activityIndicator: {
    flex: 1,
    alignSelf: "center",
    justifyContent: "center",
  },
  errorText: {
    textAlign: "center",
    padding: 20,
  },
});

const darkStyles = StyleSheet.create({
  view: {
    flex: 1,
    backgroundColor: "#111111",
  },
  scrollView: {
    flex: 1,
  },
  activityIndicator: {
    flex: 1,
    alignSelf: "center",
    justifyContent: "center",
  },
  errorText: {
    textAlign: "center",
    padding: 20,
    color: "hsl(0, 0%, 75%)",
  },
});

const mapStateToProps = (state: RootState) => ({
  fetchingSubreddits: subredditSelectors.selectFetchingSubreddits(state),
  errorFetchingSubreddits: subredditSelectors.selectErrorFetchingSubreddits(state),
  subreddits: subredditSelectors.selectSubreddits(state),
});

const mapDispatchToProps = (dispatch: Dispatch<RootAction>) =>
  bindActionCreators({
    loadDefaultSubreddits: subredditActions.loadDefaultSubredditsRequest,
  }, dispatch);

export default withDarkModeCtx(connect(mapStateToProps, mapDispatchToProps)(HomeScreen));
