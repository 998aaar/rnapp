import React, { Component } from "react";
import {
  ActivityIndicator,
  Button,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import { NavigationScreenProps } from "react-navigation";
import { connect } from "react-redux";
import { bindActionCreators, Dispatch } from "redux";

import { PostCard, withDarkModeCtx } from "../components";
import { postActions } from "../redux/modules/post/actions";
import RootAction from "../redux/rootAction";
import RootState from "../redux/rootState";
import * as postSelectors from "../selectors/post";
import { Post } from "../types/post";

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = ReturnType<typeof mapDispatchToProps>;
type SubredditScreenProps =
  StateProps & DispatchProps & NavigationScreenProps & { darkMode: boolean };

interface SubredditScreenState {
  subreddit: string;
}

class SubredditScreen extends Component<SubredditScreenProps, SubredditScreenState> {
  public componentWillMount() {
    const { navigation } = this.props;

    this.setState({ subreddit: navigation.state.params && navigation.state.params.name });
  }

  public componentDidMount() {
    const { subreddit } = this.state;
    const { posts, loadTopPostsFromSubreddit } = this.props;

    if (Object.keys(posts).length === 0) {
      loadTopPostsFromSubreddit(subreddit);
    }
  }

  public render() {
    const {
      fetchinPosts,
      errorFetchingPosts,
      posts,
    } = this.props;

    const styles = this.getStyles();

    return (
      <View style={styles.view}>
        {fetchinPosts &&
          this.renderActivityIndicator()
        }
        {!fetchinPosts && errorFetchingPosts &&
          this.renderFetchingErrroMessage()
        }
        {!fetchinPosts && Object.keys(posts).length > 0 &&
          this.renderPosts(posts)
        }
      </View>
    );
  }

  private getStyles() {
    const { darkMode } = this.props;
    return darkMode ? darkStyles : lightStyles;
  }

  private renderActivityIndicator() {
    const styles = this.getStyles();

    return (
      <View style={styles.activityIndicator}>
        <ActivityIndicator size="large" />
      </View>
    );
  }

  private renderFetchingErrroMessage() {
    const { subreddit } = this.state;
    const { loadTopPostsFromSubreddit, navigation } = this.props;
    const subredditName = navigation.state.params && navigation.state.params.name;
    const styles = this.getStyles();

    return (
      <View>
        <Text style={styles.errorText}>
          There was a problem loading posts from {subreddit}.
          Please try again.
          </Text>
        <Button
          title="Retry"
          onPress={() => loadTopPostsFromSubreddit(subredditName)} />
      </View>
    );
  }

  private renderPosts(posts: Post[]) {
    const { subreddit } = this.state;
    const { darkMode, loadTopPostsFromSubreddit, navigation } = this.props;
    const styles = this.getStyles();

    return (
      <ScrollView style={styles.scrollView}>
        <Button
          onPress={() => loadTopPostsFromSubreddit(subreddit)}
          title={`Refresh posts of ${subreddit}`} />
        {
          Object.values(posts).map((post) => (
            <PostCard
              darkMode={darkMode}
              onPress={() =>
                navigation.navigate("Post", {
                  subreddit,
                  postTitle: post.title,
                  postId: post.id,
                })}
              key={post.id}
              id={post.id}
              title={post.title}
              comments={post.comments}
              thumbnail_url={post.thumbnail_url}
              subreddit={post.subreddit}
              points={post.points} />
          ))
        }
      </ScrollView>
    );
  }
}

const lightStyles = StyleSheet.create({
  view: {
    flex: 1,
  },
  scrollView: {
    flex: 1,
  },
  activityIndicator: {
    flex: 1,
    alignSelf: "center",
    justifyContent: "center",
  },
  errorText: {
    textAlign: "center",
    padding: 20,
  },
});

const darkStyles = StyleSheet.create({
  view: {
    flex: 1,
    backgroundColor: "#111111",
  },
  scrollView: {
    flex: 1,
  },
  activityIndicator: {
    flex: 1,
    alignSelf: "center",
    justifyContent: "center",
  },
  errorText: {
    textAlign: "center",
    padding: 20,
    color: "hsl(0, 0%, 75%)",
  },
});

const mapStateToProps = (state: RootState, props: NavigationScreenProps) => ({
  fetchinPosts: postSelectors.selectFetchingPosts(state),
  errorFetchingPosts: postSelectors.selectErrorFetchingPosts(state),
  posts: postSelectors.selectSubredditPosts(state, props),
});

const mapDispatchToProps = (dispatch: Dispatch<RootAction>) =>
  bindActionCreators({
    loadTopPostsFromSubreddit: postActions.loadTopPostsFromSubredditRequest,
  }, dispatch);

export default withDarkModeCtx(connect(mapStateToProps, mapDispatchToProps)(SubredditScreen));
