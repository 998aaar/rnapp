import React, { Component } from "react";
import {
  StyleSheet,
  Switch,
  Text,
  View,
} from "react-native";
import { NavigationScreenProps } from "react-navigation";
import { connect } from "react-redux";
import { bindActionCreators, Dispatch } from "redux";

import { withDarkModeCtx } from "../components";
import { settingsActions } from "../redux/modules/settings/actions";
import RootAction from "../redux/rootAction";
import RootState from "../redux/rootState";

type DispatchProps = ReturnType<typeof mapDispatchToProps>;
type SettingsScreenProps =
  { darkMode: boolean } & DispatchProps & NavigationScreenProps;

class SettingsScreen extends Component<SettingsScreenProps> {
  public render() {
    const {
      darkMode,
      setDarkMode,
    } = this.props;

    const styles = this.props.darkMode ? darkStyles : lightStyles;

    return (
      <View style={styles.view}>
        <View style={styles.settingCard}>
          <View>
            <Text style={styles.settingTitle}>
              Dark mode
          </Text>
            <Text style={styles.settingDescription}>
              Makes the app color scheme darker so it's easier on your eyes.
          </Text>
          </View>
          <View>
            <Switch
              value={darkMode}
              onValueChange={() => setDarkMode()}
              style={{ marginTop: 10 }} />
          </View>
        </View>
      </View >
    );
  }
}

const lightStyles = StyleSheet.create({
  view: {
    flex: 1,
  },
  settingCard: {
    backgroundColor: "#ffffff",
    padding: 20,
  },
  settingTitle: {
    fontSize: 16,
    fontWeight: "bold",
    color: "#000000",
  },
  settingDescription: {
    marginTop: 10,
    fontSize: 14,
    color: "hsl(0, 0%, 50%)",
  },
});

const darkStyles = StyleSheet.create({
  view: {
    flex: 1,
    backgroundColor: "#111111",
  },
  settingCard: {
    backgroundColor: "#222222",
    padding: 20,
  },
  settingTitle: {
    fontSize: 16,
    fontWeight: "bold",
    color: "#ffffff",
  },
  settingDescription: {
    marginTop: 10,
    fontSize: 14,
    color: "hsl(0, 0%, 75%)",
  },
});

const mapDispatchToProps = (dispatch: Dispatch<RootAction>) =>
  bindActionCreators({
    setDarkMode: settingsActions.setDarkMode,
  }, dispatch);

export default withDarkModeCtx(connect(null, mapDispatchToProps)(SettingsScreen));
