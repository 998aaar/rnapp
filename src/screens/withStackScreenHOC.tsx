import React, { Component, ComponentType } from "react";
import { StatusBar } from "react-native";
import {
  NavigationLeafRoute,
  NavigationParams,
  NavigationScreenOptions,
  NavigationScreenProp,
  NavigationScreenProps,
  NavigationState,
} from "react-navigation";
import { connect } from "react-redux";

import RootState from "../redux/RootState";
import * as settingsSelectors from "../selectors/settings";

type NavigationProps =
  | NavigationScreenProp<NavigationLeafRoute<NavigationParams>
  | (NavigationLeafRoute<NavigationParams> & NavigationState), NavigationParams>;

interface NavigationOptionsProps {
  navigation: NavigationProps;
}

type StackScreenProps =
  ReturnType<typeof mapStateToProps> & NavigationScreenProps;

const withStackScreenHOC = <T extends {}>(WrappedComponent: ComponentType<T>) => {
  class StackScreen extends Component<StackScreenProps> {
    public static navigationOptions = ({ navigation }: NavigationOptionsProps): NavigationScreenOptions => {
      let title: string;

      // Set screen titles dynamically
      if (navigation.state.routeName === "Main") {
        title = "Subreddits";
      } else if (navigation.state.routeName === "Subreddit") {
        title = navigation.state.params && navigation.state.params.name;
      } else if (navigation.state.routeName === "Settings") {
        title = "Settings";
      } else if (navigation.state.routeName === "Post") {
        title = navigation.state.params && navigation.state.params.postTitle;
      } else {
        title = "Hoop";
      }

      return ({
        title,
        headerStyle: {
          backgroundColor: navigation.getParam("headerBackgroundColor"),
          borderBottomColor: navigation.getParam("headerBorderColor"),
        },
        headerTitleStyle: {
          color: navigation.getParam("headerTitleColor"),
        },
        headerBackTitleStyle: {
          color: navigation.getParam("headerTitleColor"),
        },
        headerTintColor: navigation.getParam("headerTitleColor"),
      });
    }

    public componentDidMount() {
      this.setDarkMode();

    }

    public componentDidUpdate(prevProps: StackScreenProps) {
      if (this.props.darkMode !== prevProps.darkMode) {
        this.setDarkMode();
      }
    }

    public setDarkMode() {
      const {
        darkMode,
        navigation,
      } = this.props;

      navigation.setParams({
        headerBackgroundColor: darkMode ? "#222222" : "#ffffff",
        headerBorderColor: darkMode ? "#000000" : "#aaaaaa",
        headerTitleColor: darkMode ? "#ffffff" : "#000000",
      });
    }

    public render() {
      const {
        darkMode,
      } = this.props;

      return (
        <>
          <StatusBar barStyle={darkMode ? "light-content" : "dark-content"} />
          <WrappedComponent {...this.props} />
        </>
      );
    }
  }

  return connect(mapStateToProps)(StackScreen);
};

const mapStateToProps = (state: RootState) => ({
  darkMode: settingsSelectors.selectDarkMode(state),
});

export default withStackScreenHOC;
